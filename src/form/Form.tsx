import * as React from 'react';

import Button from '@material-ui/core/Button';

import { CheckboxField } from './fields/Checkbox';
import { DateField } from './fields/Date';
import { DateTimeField } from './fields/DateTime';
import { EmailField } from './fields/Email';
import { NumberField } from './fields/Number';
import { PasswordField } from './fields/Password';
import { RadiosField } from './fields/Radios';
import { SelectField } from './fields/Select';
import { Text } from './fields/Text';
import { TextArea } from './fields/TextArea';
import { TimeField } from './fields/Time';

import { ChildForm } from './ChildForm';

export class Form extends React.Component<any, any> {
  public state: object = {};

  public fields = {
    checkbox: CheckboxField,
    date: DateField,
    datetime: DateTimeField,
    email: EmailField,
    number: NumberField,
    password: PasswordField,
    radios: RadiosField,
    select: SelectField,
    time: TimeField,
    text: Text,
    textarea: TextArea,
    subform: Form,
  };

  constructor(props: any) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  public handleSubmit(event: any) {
    const { log } = console;
    log(this.state);
    event.preventDefault();
  }
  public handleChange(formKey: string) {
    if (formKey === '') {
      return (event: any) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
      };
    }
    if (typeof this.state[formKey] === 'undefined') {
      this.state[formKey] = {};
    }
    return (event: any) => {
      const { name, value } = event.target;
      this.setState({
        [formKey]: {
          ...this.state[formKey],
          [name]: value,
        },
      });
    };
  }

  public render() {
    const { input } = this.props;
    const forms = input.map((field: any, index: number) => {
      if (field.type === 'subform') {
        return (
          <ChildForm
            input={field.items}
            label={field.label}
            handleChange={this.handleChange(field.key)}
            key={field.key}
          />
        );
      }
      const Field = this.fields[field.type];
      return (
        <Field
          label={field.label}
          name={field.key}
          key={field.key}
          handleChange={this.handleChange('')}
          helperText={field.help}
          items={field.items}
        />
      );
    });
    return (
      <div style={{ padding: '20px' }} className={'SchemaForm'}>
        <h3>Form</h3>
        <form style={{ width: '100%' }} onSubmit={this.handleSubmit}>
          {forms}
          <Button type="submit">Submit</Button>
        </form>
      </div>
    );
  }
}
