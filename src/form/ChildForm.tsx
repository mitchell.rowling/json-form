import * as React from 'react';

import { CheckboxField } from './fields/Checkbox';
import { DateField } from './fields/Date';
import { DateTimeField } from './fields/DateTime';
import { EmailField } from './fields/Email';
import { NumberField } from './fields/Number';
import { PasswordField } from './fields/Password';
import { RadiosField } from './fields/Radios';
import { SelectField } from './fields/Select';
import { Text } from './fields/Text';
import { TextArea } from './fields/TextArea';
import { TimeField } from './fields/Time';
import { IFieldElement } from './input';

export class ChildForm extends React.Component<any, any> {
  public state: object = {};

  public fields = {
    checkbox: CheckboxField,
    date: DateField,
    datetime: DateTimeField,
    email: EmailField,
    number: NumberField,
    password: PasswordField,
    radios: RadiosField,
    select: SelectField,
    time: TimeField,
    text: Text,
    textarea: TextArea,
  };

  constructor(props: any) {
    super(props);
    this.handleChange = this.props.handleChange;
  }
  public handleSubmit(event: any) {
    const { log } = console;
    log(this.state);
    event.preventDefault();
  }
  public handleChange(event: any) {
    const { name, value } = event.target;
    this.state[name] = value;
  }

  public render() {
    const { input, label } = this.props;
    const Fields = input.map((field: IFieldElement) => {
      const Field = this.fields[field.type];
      return (
        <Field
          label={field.label}
          name={field.key}
          key={field.key}
          handleChange={this.handleChange}
          helperText={field.help}
          items={field.items}
        />
      );
    });

    return (
      <div>
        <h3>{label}</h3>
        {Fields}
      </div>
    );
  }
}
