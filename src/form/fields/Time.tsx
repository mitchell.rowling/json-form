import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class TimeField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Time">
        <TextField
          label={label}
          name={name}
          type="time"
          fullWidth={true}
          helperText={helperText}
          onChange={handleChange}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    );
  }
}
