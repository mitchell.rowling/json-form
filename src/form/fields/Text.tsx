import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class Text extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Text">
        <TextField
          label={label}
          name={name}
          fullWidth={true}
          helperText={helperText}
          onChange={handleChange}
        />
      </div>
    );
  }
}
