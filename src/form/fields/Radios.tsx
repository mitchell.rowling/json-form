import * as React from 'react';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

export class RadiosField extends React.Component<any, any> {
  public state: any = {};
  public key: string;

  constructor(props: any) {
    super(props);
  }

  public handleChange = (event: any) => {
    const { handleChange, name } = this.props;
    this.setState({ value: event.target.value }, () =>
      handleChange({
        target: {
          name,
          value: this.state,
        },
      })
    );
  };

  public render() {
    const { label, name, items, helperText } = this.props;
    const radios = items.map((item: any) => {
      return (
        <FormControlLabel
          key={item.key}
          value={item.value}
          control={<Radio />}
          label={item.value}
        />
      );
    });
    return (
      <div className="Radios">
        <FormControl component="fieldset">
          <FormLabel component="legend">{label}</FormLabel>
          <RadioGroup
            aria-label={name}
            name={name}
            value={this.state.value}
            onChange={this.handleChange}
          >
            {radios}
          </RadioGroup>
          <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
      </div>
    );
  }
}
