import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class DateTimeField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="DateTime">
        <TextField
          label={label}
          name={name}
          onChange={handleChange}
          type="datetime-local"
          fullWidth={true}
          helperText={helperText}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    );
  }
}
