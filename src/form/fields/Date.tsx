import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class DateField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Date">
        <TextField
          label={label}
          name={name}
          onChange={handleChange}
          type="date"
          fullWidth={true}
          helperText={helperText}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    );
  }
}
