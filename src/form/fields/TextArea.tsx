import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class TextArea extends React.Component<any, any> {
  public render() {
    const { label, helperText, handleChange, name } = this.props;
    return (
      <div className="TextArea">
        <TextField
          id="textarea"
          label={label}
          name={name}
          multiline={true}
          fullWidth={true}
          onChange={handleChange}
          helperText={helperText}
        />
      </div>
    );
  }
}
