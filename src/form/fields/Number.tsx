import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class NumberField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Number">
        <TextField
          label={label}
          name={name}
          onChange={handleChange}
          type="number"
          fullWidth={true}
          helperText={helperText}
        />
      </div>
    );
  }
}
