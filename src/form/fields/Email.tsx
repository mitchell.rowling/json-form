import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class EmailField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Email">
        <TextField
          label={label}
          name={name}
          onChange={handleChange}
          type="email"
          fullWidth={true}
          helperText={helperText}
        />
      </div>
    );
  }
}
