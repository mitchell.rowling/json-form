import * as React from 'react';

import TextField from '@material-ui/core/TextField';

export class PasswordField extends React.Component<any, any> {
  public render() {
    const { label, handleChange, helperText, name } = this.props;
    return (
      <div className="Password">
        <TextField
          label={label}
          name={name}
          onChange={handleChange}
          type="password"
          fullWidth={true}
          helperText={helperText}
        />
      </div>
    );
  }
}
