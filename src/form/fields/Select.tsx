import * as React from 'react';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

export class SelectField extends React.Component<any, any> {
  public state: any = {
    value: '',
  };
  public handleChange = (event: any) => {
    const { handleChange, name } = this.props;
    this.setState({ value: event.target.value }, () =>
      handleChange({
        target: {
          name,
          value: this.state.value,
        },
      })
    );
  };
  public render() {
    const { label, name, items } = this.props;
    const selects = items.map((item: any) => {
      return (
        <MenuItem key={item.key} value={item.value}>
          {item.label}
        </MenuItem>
      );
    });
    return (
      <div className="Select">
        <FormControl fullWidth={true}>
          <InputLabel htmlFor={name}>{label}</InputLabel>
          <Select
            value={this.state.value}
            onChange={this.handleChange}
            inputProps={{
              name,
              id: name,
            }}
            fullWidth={true}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {selects}
          </Select>
        </FormControl>
      </div>
    );
  }
}
