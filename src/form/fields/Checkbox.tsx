import * as React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';

export class CheckboxField extends React.Component<any, any> {
  public state: any = {};
  public key: string;
  constructor(props: any) {
    super(props);
    const { items } = this.props;
    items.forEach((item: any) => {
      this.state[item.key] = item.checked || false;
    });
  }
  public handleChange = (optionName: string) => (event: any) => {
    const { handleChange, name } = this.props;
    this.setState({ [optionName]: event.target.checked }, () =>
      handleChange({
        target: {
          name,
          value: this.state,
        },
      })
    );
  };
  public render() {
    const { label, items, helperText } = this.props;
    const checkboxes = items.map((item: any) => {
      return (
        <FormControlLabel
          key={item.key}
          control={
            <Checkbox
              onChange={this.handleChange(item.key)}
              value={item.value}
            />
          }
          label={item.value}
        />
      );
    });
    return (
      <div className="Checkbox">
        <FormControl component="fieldset">
          <FormLabel component="legend">{label}</FormLabel>
          <FormGroup>
            {checkboxes}
            {/* <FormControlLabel
              control={
                <Checkbox
                  checked={gilad}
                  onChange={this.handleChange('gilad')}
                  value="gilad"
                />
              }
              label="Gilad Gray"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={jason}
                  onChange={this.handleChange('jason')}
                  value="jason"
                />
              }
              label="Jason Killian"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={antoine}
                  onChange={this.handleChange('antoine')}
                  value="antoine"
                />
              }
              label="Antoine Llorca"
            /> */}
          </FormGroup>
          <FormHelperText>{helperText}</FormHelperText>
        </FormControl>
      </div>
    );
  }
}
