// Isn't strictly JSON, but for initial development, extra TypeChecking will assist.
// This example creates an example form with an example of every element

export interface IFieldElement {
  key: string;
  label?: string;
  type:
    | 'checkbox'
    | 'date'
    | 'datetime'
    | 'email'
    | 'number'
    | 'password'
    | 'radios'
    | 'select'
    | 'time'
    | 'text'
    | 'textarea';
  placeholder?: string;
  help?: string;
  items?: any;
}

export interface IChildForm {
  key: string;
  label: string;
  items: IFieldElement[];
}

// export const input: IChildForm[] = [
export const input: any[] = [
  {
    key: 'test',
    label: 'test',
    type: 'text',
  },
  {
    key: 'guardians',
    label: 'guardians',
    type: 'subform',
    items: [
      {
        key: 'checkbox',
        label: 'checkbox-label',
        type: 'checkbox',
        help: 'checkbox-help-text',
        items: [
          {
            key: 'option1',
            label: 'Option 1',
            value: 'test1',
          },
          {
            key: 'option2',
            label: 'Option 2',
            value: 'test2',
          },
        ],
      },
      {
        key: 'date',
        label: 'date-label',
        type: 'date',
        help: 'date-help-text',
      },
      {
        key: 'datetime',
        label: 'datetime-label',
        type: 'datetime',
        help: 'datetime-help-text',
      },
      {
        key: 'time',
        label: 'time-label',
        type: 'time',
        help: 'time-help-text',
      },
      {
        key: 'email',
        label: 'email-label',
        type: 'email',
        help: 'email-help-text',
      },
      {
        key: 'number',
        label: 'number-label',
        type: 'number',
        help: 'number-help-text',
      },
      {
        key: 'password',
        label: 'password-label',
        type: 'password',
        help: 'password-help-text',
      },
      {
        key: 'radios',
        label: 'radios-label',
        type: 'radios',
        help: 'radios-help-text',
        items: [
          {
            key: 'option1',
            label: 'Option 1',
            value: 'test1',
          },
          {
            key: 'option2',
            label: 'Option 2',
            value: 'test2',
          },
        ],
      },
      {
        key: 'select',
        label: 'select-label',
        type: 'select',
        help: 'select-help-text',
        items: [
          {
            key: 'option1',
            label: 'Option 1',
            value: 'test1',
          },
          {
            key: 'option2',
            label: 'Option 2',
            value: 'test2',
          },
        ],
      },
    ],
  },
  {
    key: 'guardians2',
    label: 'guardians2',
    type: 'subform',
    items: [
      {
        key: 'text',
        label: 'text-label',
        type: 'text',
        help: 'text-help-text',
      },
      {
        key: 'textarea',
        label: 'textarea-label',
        type: 'textarea',
        help: 'textarea-help-text',
      },
    ],
  },
];
