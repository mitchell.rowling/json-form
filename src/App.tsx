import * as React from 'react';
import './App.css';

import { Form } from './form/Form';

import { input } from './form/input';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">JSON Form</h1>
        </header>
        <Form input={input} />
      </div>
    );
  }
}

export default App;
